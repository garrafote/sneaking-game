﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.Engine;
using System.Collections.Generic;

[RequireComponent(typeof(BoxCollider))]
public class Quadrant : MonoBehaviour {

    public BoxCollider boxCollider;

    public IList<Node> nodes;

	void Awake () {
        var gm = ScriptableResource.Load<GameManager>("Manager");

        nodes = new List<Node>();
        boxCollider = (BoxCollider)collider;

        foreach (var node in gm.Nodes)
        {
            if (boxCollider.bounds.Contains(node.transform.position))
            {
                nodes.Add(node);
            }
        }

        Debug.Log(nodes.Count);
	}

    
}
