﻿using UnityEngine;
using System.Linq;
using System.Collections;

public class Node : MonoBehaviour {

    public Transform[] next;
    public Transform[] prev;

    public Transform[] adjacent { get { return next.Union(prev).ToArray(); } }

    public IElement element;

    void OnDrawGizmos()
    {
        Gizmos.color = Color.magenta;
        foreach (var item in next)
        {
            var vec = (item.position - transform.position);
            var dir = vec.normalized;
            var size = vec.magnitude * .5f;
            Gizmos.DrawLine(transform.position, transform.position + (dir * size));
        }

        Gizmos.color = Color.yellow;
        foreach (var item in prev)
        {
            var vec = (item.position - transform.position);
            var dir = vec.normalized;
            var size = vec.magnitude * .5f;
            Gizmos.DrawLine(transform.position, transform.position + (dir * size));
        }




    }

    void OnMouseDown()
    {
        Debug.Log("hey");
    }

}
