﻿using UnityEngine;
using System.Linq;
using System.Collections.Generic;
using Assets.Scripts.Engine;

public class GraphTest : MonoBehaviour {
    public Material defaultMaterial;
    public Material guardMaterial;
    public Material playerMaterial;
    public Material candidateMaterial;

    GameManager gm;

    void Start()
    {
        gm = GameManager.Instance;
        gm.StartGame(2);

        Redraw();

        gm.Invalidate += Redraw;
    }

    void Redraw()
    {

        foreach (Transform item in transform)
        {
            item.transform.Find("Mesh").GetComponent<MeshRenderer>().material = defaultMaterial;
        }

        foreach (var player in gm.players)
        {
            player.CurrentNode.transform.Find("Mesh").GetComponent<MeshRenderer>().material = playerMaterial;
        }

    }

}
