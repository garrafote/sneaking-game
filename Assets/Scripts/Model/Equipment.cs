﻿using UnityEngine;
using System.Collections.Generic;
using System;

[Serializable]
public class Equipment {

    public string ID;
    public string Name;
    public string Description;
    public string[] RequiredResources;
}
