﻿using UnityEngine;
using System.Collections.Generic;

public class Player {

    public IList<Equipment> Equipments { get; private set; }
    public IList<Resource> Resources { get; private set; }
    public Node CurrentNode { get; set; }

    public Player()
    {
        Equipments = new List<Equipment>();
        Resources = new List<Resource>();
    }

}
