﻿using UnityEngine;
using System.Collections;
using System;

public interface IElement
{

}

public struct Guard : IElement
{

}

public struct Dog : IElement
{

}

[Serializable]
public struct Resource : IElement
{

    public string ID;
    public string Name;
    public string Description;
}

