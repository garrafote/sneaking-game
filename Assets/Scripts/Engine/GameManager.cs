﻿using UnityEngine;
using System;
using System.Linq;
using System.Collections.Generic;
using Assets.Scripts.Engine;
using System.Text.RegularExpressions;

public class GameManager : ScriptableObject {

    enum TurnPhase
    {
        Move,
        Act,
    }

    public Resource[] resources;
    public Equipment[] equipments;

    public int numberOfGuards;
    public int numberOfDogs;

    public Guard[] guards;
    public Dog[] dogs;

    [NonSerialized]
    public Node[] guardNodes;

    [NonSerialized]
    public Player[] players;

    [NonSerialized]
    public Quadrant[] quadrants;

    int turn;
    TurnPhase phase;

    public event Action Invalidate;

    private static GameManager instance;
    public static GameManager Instance
    {
        get
        {
            if (instance == null)
                instance = ScriptableResource.Load<GameManager>("Manager");

            return instance;
        }
    }
    
    private IList<Node> nodes;
    public IList<Node> Nodes
    {
        get
        {
            if (nodes == null)
            {
                var nodeList = new List<Node>();

                var container = GameObject.Find("Level");
                var children = FlattenChildren(new List<Transform>(), container.transform);
                foreach (Transform child in children)
                {
                    var node = child.GetComponent<Node>();
                    if (node != null)
                        nodeList.Add(node);
                }

                nodes = nodeList;
            }
            return nodes;
        }
    }

    IList<Transform> FlattenChildren(IList<Transform> list, Transform root)
    {
        foreach (Transform child in root.transform)
        {
            list.Add(child);
            FlattenChildren(list, child);
        }

        return list;
    }

    public void StartGame(int numPlayers)
    {
        players = new Player[numPlayers];
        for (int i = 0; i < numPlayers; i++)
        {
            players[i] = new Player();
        }

        var container = GameObject.Find("Quadrants");
        quadrants = container.GetComponentsInChildren<Quadrant>();

        guards = new Guard[numberOfGuards];
        dogs = new Dog[numberOfDogs];
        resources = new Resource[6];

        int resourcesPerQuadrant = (resources.Count() / quadrants.Count());
        int guardsPerQuadrant = (guards.Count() / quadrants.Count());
        int dogsPerQuadrant = (dogs.Count() / quadrants.Count());
        for (int i =0; i<quadrants.Count(); i++) {
            var quadrantResources = resources.Skip(i * resourcesPerQuadrant).Take(resourcesPerQuadrant).ToList();
            var quadrantGuards = guards.Skip(i * guardsPerQuadrant).Take(guardsPerQuadrant).ToList();
            var quadrantDogs = dogs.Skip(i * dogsPerQuadrant).Take(dogsPerQuadrant).ToList();
            initializeQuadrant(quadrants[i].nodes, quadrantResources, quadrantGuards, quadrantDogs);
        }

        var pattern = @"P(?<id>\d+) Spawn";
        var remaining = numPlayers;

        for (int i = 0; i < Nodes.Count; i++)
        {
            var node = Nodes[i];

            var match = Regex.Match(node.tag, pattern);
            if (match.Success)
            {
                var pIndex = Convert.ToInt32(match.Groups["id"].Value) - 1;
                if (pIndex >= numPlayers)
                    continue;

                players[pIndex].CurrentNode = node;
                remaining -= 1;
            }

            if (remaining == 0)
            {
                break;
            }
        }
    }

    public void initializeQuadrant(IList<Node> nodes, IList<Resource> resources, IList<Guard> guards, IList<Dog> dogs)
    {
        System.Diagnostics.Debug.Assert(nodes.Count == resources.Count + guards.Count + dogs.Count);

        var list = nodes.ToArray().ToList();
        for (int i = 0; i < guards.Count; i++)
        {
            var node = list[UnityEngine.Random.Range(0, list.Count)];
            node.element = guards[i];
            list.Remove(node);
        }

        for (int i = 0; i < dogs.Count; i++)
        {
            var node = list[UnityEngine.Random.Range(0, list.Count)];
            node.element = dogs[i];
            list.Remove(node);
        }

        while (list.Count > 0 && resources.Count > 0)
        {
            var node = list.Last();
            node.element = resources.Last();
            list.Remove(node);
            resources.Remove(resources.Last());
        }
    }


    public void HandleNodeClicked(Node node)
    {
        // ensure that we are in move phase
        if (phase != TurnPhase.Move)
        {
            return;
        }

        var player = players[turn];

        // node needs to be adjacent to the player
        if (!player.CurrentNode.adjacent.Select(t => t.GetComponent<Node>()).Contains(node))
        {
            return;
        }

        // node can't contain a guard
        //if (guardNodes.Contains(node))
        //{
        //    return;
        //}

        // set player node state to explored
        //player.CurrentNode.transform.Find("Mesh").GetComponent<MeshRenderer>().material = defaultMaterial;

        if (node.element == null)
        {
            // empty node, move freely
            player.CurrentNode = node;
        }
        else if (node.element is Resource)
        {
            // nice! move and get a resource
            player.CurrentNode = node;

            var resource = (Resource)node.element;
            player.Resources.Add(resource);
            node.element = null;
        }
        else if (node.element is Guard)
        {
            // found a guard.. what to do?

        }
        else if (node.element is Dog)
        {
            // found a dog... guard nearby!
            player.CurrentNode = node;

            var candidates = new List<Node>();
            AddCandidates(node, candidates);

            var newGuardNode = candidates.ToList()[UnityEngine.Random.Range(0, candidates.Count)];
            //guards.Add(newGuard);
            //newGuard.transform.Find("Mesh").GetComponent<MeshRenderer>().material = guardMaterial;
        }

        if (Invalidate != null) Invalidate();

        turn += 1;
        turn = turn % players.Length;
    }

    void AddCandidates(Node node, IList<Node> candidates)
    {
        foreach (var nextTransform in node.next)
        {
            var nextNode = nextTransform.GetComponent<Node>();

            AddCandidates(nextNode, candidates);
            candidates.Add(nextNode);
        }
    }

    public void HandlePass()
    {

    }

    public void HandleEquipmentClicked(Equipment equipment)
    {

    }

    public void HandleCraftEquipmentClicked(Player player)
    { 

    }
       
}
