﻿using UnityEngine;
using System.Collections;

public class LevelManager : MonoBehaviour {

    public Transform[] playerPawns;

    GameManager gm;

    void Awake()
    {
        gm = GameManager.Instance;
        gm.StartGame(4);

        Redraw();

        for (int i = 0; i < gm.players.Length; i++)
        {
            playerPawns[i].transform.position = gm.players[i].CurrentNode.transform.position + new Vector3(0, 0, -1);
        }

        gm.Invalidate += Redraw;
    }

    void Redraw()
    {
        Debug.Log("redraw");
        for (int i = 0; i < gm.players.Length; i++)
        {
            playerPawns[i].transform.position = gm.players[i].CurrentNode.transform.position + new Vector3(0, 0, -1);
        }

    }
}
