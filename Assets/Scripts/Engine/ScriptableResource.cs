﻿using System;
using System.IO;
using System.Linq;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Assets.Scripts.Engine
{
    public static class ScriptableResource
    {
        public static TObj Load<TObj>(string resourcePath) where TObj : ScriptableObject
        {
            TObj instance = null;

            try
            {
                instance = (TObj)Resources.Load(resourcePath, typeof(TObj));

#if UNITY_EDITOR
				if (instance == null)
				{
					
					var split = resourcePath.Split(new[] {'/', '\\'}).ToList();
					split.RemoveAt(split.Count - 1);
					var path = Path.Combine(Application.dataPath + "/Resources", string.Join("/", split.ToArray()));
					

					//If the settings asset doesn't exist, then create it. We require a resources folder
					if (!Directory.Exists(path))
					{
						Directory.CreateDirectory(path);
					}

					var asset = ScriptableObject.CreateInstance<TObj>();

					//some hack to move the asset around
					//string path = AssetDatabase.GetAssetPath(Selection.activeObject);
					//if (path == "")
					//{
					//    path = "Assets";
					//}
					//else if (Path.GetExtension(path) != "")
					//{
					//    path = path.Replace(Path.GetFileName(AssetDatabase.GetAssetPath(Selection.activeObject)), "");
					//}

					var relativePath = Path.Combine("Assets/Resources", resourcePath).Replace('\\', '/') + ".asset";
					string uniquePath = AssetDatabase.GenerateUniqueAssetPath(relativePath);
					AssetDatabase.CreateAsset(asset, uniquePath);
					
					if (uniquePath != relativePath)
					{
						Debug.LogWarning("Bottle: The path '" + relativePath + "' used to save the settings file is not available.");
					}

					AssetDatabase.SaveAssets();

					Debug.LogWarning("Bottle: Settings file didn't exist and was created");

					Selection.activeObject = asset;

					//save reference
					instance = asset;
				}
#endif
            }
            catch (Exception e)
            {
                Debug.Log("Error getting " + typeof(TObj).Name + "in Load: " + e.Message);
            }

            return instance;
        }
    }
}